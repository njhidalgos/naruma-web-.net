﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Naruma.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
       
        public ActionResult Nosotros()
        {
            ViewBag.Title = "Nosotros";

            return View();
        }
        public ActionResult Proyectos()
        {
            ViewBag.Title = "Proyectos";

            return View();
        }
        public ActionResult Contacto()
        {
            ViewBag.Title = "Contacto";

            return View();
        }
    }
}
